function init(){

    var mymap = L.map('map', { center: [4.547603, -74.097312], zoom:13});
    
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
         maxZoom: 20,
        id: 'mapbox.streets', 
    }).addTo(mymap);
    
    var customIcon = new L.Icon({

        iconUrl: 'https://cdn.icon-icons.com/icons2/1559/PNG/512/3440906-direction-location-map-marker-navigation-pin_107531.png',
        iconSize: [50, 50],
        iconAnchor: [50, 50]

      });
      
    let lugares = ["Parque Simón Bolivar", "Parque el Salitre", "Parque Tercer Milenio", "Panadería La Victoria"];
    let coordenadas = ["4.658426,-74.094520","4.664991,-74.089181", "4.597161,-74.081012", "4.549973,-74.092433" ]
    
        for(let i=0; i<lugares.length;i++){

            let punto = coordenadas[i].split(",");
            var markeri=L.marker([parseFloat(punto[0]),parseFloat(punto[1])], {icon: customIcon}).addTo(mymap);
            markeri.bindPopup(lugares[i]);
            
        }
    }
    